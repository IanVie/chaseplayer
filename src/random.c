/*H**********************************************************************
 *
 *	This is a skeleton to guide development of Othello engines to be used
 *	with the Distributed Tournament Engine (DTE). DTE runs tournaments between
 *	game engines and displays the results on a web page which is available on
 *	campus (!!!INSERT URL!!!). Any output produced by your implementations will
 *	be available for download on the web page.
 *
 *	The socket communication required for DTE is handled in the main method,
 *	which is provided with the skeleton. All socket communication is performed
 *	at rank 0.
 *
 *	Board co-ordinates for moves start at the top left corner of the board i.e.
 *	if your engine wishes to place a piece at the top left corner, the "gen_move"
 *	function must return "00".
 *
 *	The match is played by making alternating calls to each engine's "gen_move"
 *	and "play_move" functions. The progression of a match is as follows:
 *		1. Call gen_move for black player
 *		2. Call play_move for white player, providing the black player's move
 *		3. Call gen move for white player
 *		4. Call play_move for black player, providing the white player's move
 *		.
 *		.
 *		.
 *		N. A player makes the final move and "game_over" is called for both players
 *
 *H***********************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<mpi.h>

const int EMPTY = 0;
const int WHITE = 1;
const int BLACK = 2;
const int OUTER = 3;
const int ALLDIRECTIONS[8]={-11, -10, -9, -1, 1, 9, 10, 11};
const int BOARDSIZE=100;

char* gen_move();
void play_move(char *move);
void game_over();
void run_worker();
void initialise();

int* initialboard(void);
int *legalmoves ();
int legalp (int move, int player);
int validp (int move);
int wouldflip (int move, int dir, int player);
int opponent (int player);
int findbracketingpiece(int square, int dir, int player);
int randomstrategy();
void makemove (int move, int player);
void makeflips (int move, int dir, int player);
int get_loc(char* movestring);
char* get_move_string(int loc);
void printboard();
char nameof(int piece);
int count (int player, int * board);

int my_colour;
int time_limit;
int running;
int rank;
int size;
int *board;

int main(int argc , char *argv[]) {
	int socket_desc, port, msg_len;
	char *ip, *cmd, *opponent_move, *my_move;
	char msg_buf[15], len_buf[2];
    struct sockaddr_in server;

	MPI_Init(&argc, &argv);	/* starts MPI */
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);	/* get current process id */
    MPI_Comm_size(MPI_COMM_WORLD, &size);	/* get number of processes */
	ip = argv[1];
    printf("%s\n", ip);
    port = atoi(argv[2]);
    printf("\n%d\n", port);
    time_limit = atoi(argv[3]);
    my_colour = EMPTY;
	srand(time(NULL));
    puts("Initialising board...\n");
    initialise();
	// Rank 0 is responsible for handling communication with the server
	if (rank == 0){
        puts("Getting socket_desc...\n");
	    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	    if (socket_desc == -1) {
		    printf("Could not create socket");
		    return -1;
	    }
        puts("Getting  inet_addr...\n");
	    server.sin_addr.s_addr = inet_addr(ip);
        puts("Getting family...\n");
	    server.sin_family = AF_INET;
        puts("Getting port...\n");
	    server.sin_port = htons(port);

	    //Connect to remote server
        puts("Connecting to server...\n");
	    if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0){
		    puts("connect error");
		    return -1;
	    }
	    puts("Connected");
		if (socket_desc == -1){
			return 1;
		}
		while (running == 1){
			if(recv(socket_desc, len_buf , 2, 0) < 0){
				puts("Receive failed");
				running = 0;
				break;
			}
			msg_len = atoi(len_buf);
			if(recv(socket_desc, msg_buf, msg_len, 0) < 0){
				puts("Receive failed");
				running = 0;
				break;
			}
			msg_buf[msg_len] = '\0';
			cmd = strtok(msg_buf, " ");
			puts(cmd);
			if (strcmp(cmd, "game_over") == 0){
				puts("Game over");
				running = 0;
				break;
			} else if (strcmp(cmd, "gen_move") == 0){
				my_move = gen_move();
				puts(my_move);
				if (send(socket_desc, my_move, strlen(my_move) , 0) < 0){
					puts("Move send failed");
					running = 0;
					break;
				}
			} else if (strcmp(cmd, "play_move") == 0){
				opponent_move = strtok(NULL, " ");
				play_move(opponent_move);
			}
			memset(len_buf, 0, 2);
			memset(msg_buf, 0, 15);
			printboard();
		}
		game_over();
	} else {
		run_worker(rank);
	}
	return 0;
}

/*
	Called at the start of execution on all ranks
 */
void initialise(){
	int i;
	running = 1;
	board = (int *)malloc(BOARDSIZE * sizeof(int));
	for (i = 0; i<=9; i++) board[i]=OUTER;
	for (i = 10; i<=89; i++) {
		if (i%10 >= 1 && i%10 <= 8) board[i]=EMPTY; else board[i]=OUTER;
	}
	for (i = 90; i<=99; i++) board[i]=OUTER;
	board[44]=WHITE; board[45]=BLACK; board[54]=BLACK; board[55]=WHITE;
}

/*
	Called at the start of execution on all ranks except for rank 0.
	This is where messages are passed between workers to guide the search.
 */
void run_worker(){
MPI_Finalize();    
}

/*
	Called when your engine is required to make a move. It must return
	a string of the form "xy", where x and y represent the row and
	column where your piece is placed, respectively.

	play_move will not be called for your own engine's moves, so you
	must apply the move generated here to any relevant data structures
	before returning.
 */
char* gen_move(){
	int loc;
    char* move;
	if (my_colour == EMPTY){
		my_colour = BLACK;
	}
	loc = randomstrategy();
    if (loc == -1){
        move = "pass\n";
    } else {
		move = get_move_string(loc);
		makemove(loc, my_colour);
	}
	return move;
}

/*
	Called when the other engine has made a move. The move is given in a
	string parameter of the form "xy", where x and y represent the row
	and column where the opponent's piece is placed, respectively.
 */
void play_move(char *move){
	int loc;
	if (my_colour == EMPTY){
		my_colour = WHITE;
	}
    if (strcmp(move, "pass") == 0){
        return;
    }
	loc = get_loc(move);
	makemove(loc, opponent(my_colour));
}

/*
	Called when the match is over.
 */
void game_over(){
	MPI_Finalize();
}

char* get_move_string(int loc){
	static char ms[3];
	int row, col, new_loc;
	new_loc = loc - (9 + 2 * (loc / 10));
	row = new_loc / 8;
	col = new_loc % 8;
	ms[0] = row + '0';
	ms[1] = col + '0';
	ms[2] = '\n';
	return ms;
}

int get_loc(char* movestring){
	int row, col;
	row = movestring[0] - '0';
	col = movestring[1] - '0';
	return (10 * (row + 1)) + col + 1;
}

int *legalmoves (int player) {
	int move, i, * moves;
	moves = (int *)malloc(65 * sizeof(int));
	moves[0] = 0;
	i = 0;
	for (move=11; move<=88; move++)
		if (legalp(move, player)) {
			i++;
			moves[i]=move;
		}
	moves[0]=i;
	return moves;
}

int legalp (int move, int player) {
	int i;
	if (!validp(move)) return 0;
	if (board[move]==EMPTY) {
		i=0;
		while (i<=7 && !wouldflip(move, ALLDIRECTIONS[i], player)) i++;
		if (i==8) return 0; else return 1;
	}
	else return 0;
}

int validp (int move) {
	if ((move >= 11) && (move <= 88) && (move%10 >= 1) && (move%10 <= 8))
		return 1;
	else return 0;
}

int wouldflip (int move, int dir, int player) {
	int c;
	c = move + dir;
	if (board[c] == opponent(player))
		return findbracketingpiece(c+dir, dir, player);
	else return 0;
}

int findbracketingpiece(int square, int dir, int player) {
	while (board[square] == opponent(player)) square = square + dir;
	if (board[square] == player) return square;
	else return 0;
}

int opponent (int player) {
	switch (player) {
	case 1: return 2;
	case 2: return 1;
	default: printf("illegal player\n"); return 0;
	}
}

int randomstrategy() {
	int r, * moves;
	moves = legalmoves(my_colour);
    if (moves[0] == 0){
        return -1;
    }
	r = moves[(rand() % moves[0]) + 1];
	free(moves);
	return(r);
}

void makemove (int move, int player) {
	int i;
	board[move] = player;
	for (i=0; i<=7; i++) makeflips(move, ALLDIRECTIONS[i], player);
}

void makeflips (int move, int dir, int player) {
	int bracketer, c;
	bracketer = wouldflip(move, dir, player);
	if (bracketer) {
		c = move + dir;
		do {
			board[c] = player;
			c = c + dir;
		} while (c != bracketer);
	}
}

void printboard(){
	int row, col;
	printf("    1 2 3 4 5 6 7 8 [%c=%d %c=%d]\n",
			nameof(BLACK), count(BLACK, board), nameof(WHITE), count(WHITE, board));
	for (row=1; row<=8; row++) {
		printf("%d  ", 10*row);
		for (col=1; col<=8; col++)
			printf("%c ", nameof(board[col + (10 * row)]));
		printf("\n");
	}
}


char nameof (int piece) {
	static char piecenames[5] = ".wb?";
	return(piecenames[piece]);
}

int count (int player, int * board) {
	int i, cnt;
	cnt=0;
	for (i=1; i<=88; i++)
		if (board[i] == player) cnt++;
	return cnt;
}


