/*H**********************************************************************
 *      17332001
 *	This is a skeleton to guide development of Othello engines to be used
 *	with the Distributed Tournament Engine (DTE). DTE runs tournaments between
 *	game engines and displays the results on a web page which is available on
 *	campus (bladevm1.cs.sun.ac.za:8080/DTE). Any output produced by your implementations will
 *	be available for download on the web page.
 *
 *	The socket communication required for DTE is handled in the main method,
 *	which is provided with the skeleton. All socket communication is performed
 *	at rank 0.
 *
 *	Board co-ordinates for moves start at the top left corner of the board i.e.
 *	if your engine wishes to place a piece at the top left corner, the "gen_move"
 *	function must return "00".
 *
 *H***********************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<mpi.h>
#include<time.h>

/***/
const int EMPTY = 0;
const int WHITE = 2;
const int BLACK = 1;
const int OUTER = 3;
const int WIN=2000;
const int LOSS= -2000;
#define send_data_tag 2001
#define return_data_tag 2002
MPI_Status status;

const int ALLDIRECTIONS[8]={-11, -10, -9, -1, 1, 9, 10, 11};
const int BOARDSIZE=100;
int running;

long int BOARDS;
char* gen_move();
void play_move(char *move);
int get_socket_desc(char* ip, int port);
void game_over();
void run_worker(int rank);
void initialise();


int *legalmoves(int player, int *board);
int legalp (int move, int player, int * board);
int validp (int move);
int wouldflip (int move, int player, int * board, int dir);
int opponent (int player);
int findbracketingpiece(int square, int player, int * board, int dir);
void makemove (int move, int player, int *board);
int nexttoplay (int * board, int previousplayer, int printflag);
int * copyboard (int * board);
int count (int player, int * board);
char nameof(int piece);
void printboard();
int mystrategy(int player, int * board);
char* get_move_string(int loc);
int get_loc(char* movestring);
void makeflips (int move, int player, int * board, int dir);
int maxweighteddiffstrategy1(int player, int * board);
int minmax (int player, int * board, int ply, int (* evalfn) (int, int *));
int anylegalmove (int player, int * board);
int maxchoice (int, int *, int, int (*) (int, int *));
int minchoice (int, int *, int, int (*) (int, int *));

#define max_rows 100000
int my_id, root_process, ierr, i, num_rows, num_procs,
an_id, num_rows_to_receive, avg_rows_per_process, 
sender, num_rows_received, start_row, end_row, num_rows_to_send,local_max;

long int sum;
MPI_Status status;

int *array;
int array2[max_rows];
int player;
int time_limit;
int rank;
int size;
int *board;

int main(int argc , char *argv[]) {
	int socket_desc, port, msg_len;
	char *ip, *cmd, *opponent_move, *my_move;
	char msg_buf[15], len_buf[2];

	ip = argv[1];
	port = atoi(argv[2]);
	player = 0;

	MPI_Init(&argc, &argv);	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	ip = argv[1];
	printf("%s\n", ip);
	port = atoi(argv[2]);
	printf("\n%d\n", port);
	time_limit = atoi(argv[3]);
	player = EMPTY;
	puts("Initialising board...\n");
	initialise();

	// Rank 0 is responsible for handling communication with the server
	if (rank == 0){
		socket_desc = get_socket_desc(ip, port);
		if (socket_desc == -1){
			return 1;
		}
		while (1){
			/* receive the length of message in len_buf */ 
			if(recv(socket_desc, len_buf , 2, 0) < 0){
				puts("Receive failed");
				break;
			}
			/* receive the message in msg_buf */ 
			msg_len = atoi(len_buf);
			if(recv(socket_desc, msg_buf , msg_len, 0) < 0){
				puts("Receive failed");
				break;
			}

			msg_buf[msg_len] = '\0';
			cmd = strtok(msg_buf, " ");
			puts(cmd);
			if (strcmp(cmd, "game_over") == 0){
				/* game over */
				puts("Game over");
				break;
			} else if (strcmp(cmd, "gen_move") == 0){
				/* my turn to make a move */
				if (player == 0){
					player = 1;
				}
				my_move = gen_move(); 

				/* send my move to the server */
				if (send(socket_desc , my_move , strlen(my_move) , 0) < 0){
					puts("Move send failed");
					break;
				}
			} else if (strcmp(cmd, "play_move") == 0){
				/* other player's turn to make a move */
				if (player == 0){
					player = 2;
				}
				opponent_move = strtok(NULL, " ");
				play_move(opponent_move);
			}
			memset(len_buf, 0, 2);
			memset(msg_buf, 0, 15);
			printboard();
		}
		game_over();
	} else {
		run_worker(rank);
	}
	return 0;
}

int diffeval (int player, int * board) { 
	int i, ocnt, pcnt, opp;                
	pcnt=0; ocnt = 0;                      
	opp = opponent(player);
	for (i=1; i<=88; i++) {
		if (board[i]==player) pcnt++;
		if (board[i]==opp) ocnt++;
	}
	return (pcnt-ocnt);
}

int weighteddiffeval (int player, int * board) {
	int i, ocnt, pcnt, opp;
	const int weights[100]={0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,120,-20, 20,  5,  5, 20,-20,120,  0,
			0,-20,-40, -5, -5, -5, -5,-40,-20,  0,
			0, 20, -5, 15,  3,  3, 15, -5, 20,  0,
			0,  5, -5,  3,  3,  3,  3, -5,  5,  0,
			0,  5, -5,  3,  3,  3,  3, -5,  5,  0,
			0, 20, -5, 15,  3,  3, 15, -5, 20,  0,
			0,-20,-40, -5, -5, -5, -5,-40,-20,  0,
			0,120,-20, 20,  5,  5, 20,-20,120,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0};
	pcnt=0; ocnt=0;
	opp = opponent(player);
	for (i=1; i<=88; i++) {
		if (board[i]==player) pcnt=pcnt+weights[i];
		if (board[i]==opp) ocnt=ocnt+weights[i];
	}
	return (pcnt - ocnt);
}

int nexttoplay (int * board, int previousplayer, int printflag) {
	int opp;
	opp = opponent(previousplayer);
	if (anylegalmove(opp, board)) return opp;
	if (anylegalmove(previousplayer, board)) {
		if (printflag) printf("%c has no moves and must pass.\n", nameof(opp));
		return previousplayer;
	}
	return 0;
}

int anylegalmove (int player, int * board) {
	int move;
	move = 11;
	while (move <= 88 && !legalp(move, player, board)) move++;
	if (move <= 88) return 1; else return 0;
}

int get_socket_desc(char* ip, int port){

	int socket_desc;
	struct sockaddr_in server;
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1) {
		printf("Could not create socket");
		return -1;
	}

	server.sin_addr.s_addr = inet_addr(ip);
	server.sin_family = AF_INET;
	server.sin_port = htons(port);

	//Connect to remote server
	if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0){
		puts("Connect error");
		return -1;
	}
	puts("Connected");
	return socket_desc;
}

/*
	Called at the start of execution on all ranks except for rank 0.
	This is where messages are passed between workers to guide the search.
 */
void run_worker(int rank){

	MPI_Recv( &num_rows_to_receive, 1, MPI_INT, 
			0, send_data_tag, MPI_COMM_WORLD, &status);

	MPI_Recv( &array2, num_rows_to_receive, MPI_INT, 
			0, send_data_tag, MPI_COMM_WORLD, &status);

	num_rows_received = num_rows_to_receive;


	local_max = mystrategy(player,board);

	/*send local max to processor zerolocal_max*/

	MPI_Send( &local_max, 1, MPI_INT, 0, 
			return_data_tag, MPI_COMM_WORLD);

}

/*
	Called when your engine is required to make a move. It must return
	a string of the form "xy", where x and y represent the row and
	column where your piece is placed, respectively.

	play_move will not be called for your own engine's moves, so you
	must apply the move generated here to any relevant data structures
	before returning.
 */
char* gen_move(){
	printf("in gen_move\n");
	char* move;
	int loc;
	if (player == EMPTY){
		player = BLACK;
	}
	num_rows= legalmoves (player,board)[0];
	avg_rows_per_process = num_rows / size;
	if(avg_rows_per_process>=1){
		avg_rows_per_process=1;
	}

	array = legalmoves (player,board);
	for(an_id = 1; an_id < size; an_id++) {
		start_row = an_id*avg_rows_per_process + 1;
		end_row   = (an_id + 1)*avg_rows_per_process;

		if((num_rows - end_row) < avg_rows_per_process)
			end_row = num_rows - 1;

		num_rows_to_send = end_row - start_row + 1;

		MPI_Send( &num_rows_to_send, 1 , MPI_INT,
				an_id, send_data_tag, MPI_COMM_WORLD);

		MPI_Send( &array[start_row], num_rows_to_send, MPI_INT,
				an_id, send_data_tag, MPI_COMM_WORLD);
	}

	loc = mystrategy(player,board);

	for(an_id = 1; an_id < num_procs; an_id++) {

		MPI_Recv( &local_max, 1, MPI_INT, MPI_ANY_SOURCE,
				return_data_tag, MPI_COMM_WORLD, &status);  
		sender = status.MPI_SOURCE;
		printf("Local max %d \n",local_max);
	}
	//loc = mystrategy(player,board);

	printf("The Final move %d\n",loc);
	move = get_move_string(loc);
	makemove(loc, player,board);
	return move;

}

/*
	Called when the other engine has made a move. The move is given in a
	string parameter of the form "xy", where x and y represent the row
	and column where the opponent's piece is placed, respectively.
 */
void play_move(char *move){ 
	int loc;
	if (player == EMPTY){
		player = WHITE;
	}
	loc = get_loc(move);
	makemove(loc, opponent(player),board);

}

int mystrategy(int player, int * board) {


	return(minmax(player, board, 1, diffeval));
}


char* get_move_string(int loc){
	static char ms[3];
	int row, col, new_loc;
	new_loc = loc - (9 + 2 * (loc / 10));
	row = new_loc / 8;
	col = new_loc % 8;
	ms[0] = row + '0';
	ms[1] = col + '0';
	ms[2] = '\n';
	return ms;
}

void makemove (int move, int player, int * board) {
	int i;
	board[move] = player;
	for (i=0; i<=7; i++) makeflips(move, player, board, ALLDIRECTIONS[i]);
}

int get_loc(char* movestring){
	int row, col;
	row = movestring[0] - '0';
	col = movestring[1] - '0';
	return (10 * (row + 1)) + col + 1;
}

int opponent (int player) {
	switch (player) {
	case 1: return 2;
	case 2: return 1;
	default: printf("illegal player\n"); return 0;
	}
}

int * legalmoves (int player, int * board) {
	int move, i, * moves;
	moves = (int *)malloc(65 * sizeof(int));
	moves[0] = 0;
	i = 0;
	printf("Player %d In possible moves\n",player);
	for (move=11; move<=88; move++)
		if (legalp(move, player, board)) {
			i++;
			moves[i]=move;
			printf("%d ",move);
		}
	moves[0]=i;
	printf("\n");

	return moves;
}

void makeflips (int move, int player, int * board, int dir) {
	int bracketer, c;
	bracketer = wouldflip(move, player, board, dir);
	if (bracketer) {
		c = move + dir;
		do {
			board[c] = player;
			c = c + dir;
		} while (c != bracketer);
	}
}

int legalp (int move, int player, int * board) {
	int i;
	if (!validp(move)) return 0;
	if (board[move]==EMPTY) {
		i=0;
		while (i<=7 && !wouldflip(move, player, board, ALLDIRECTIONS[i])) i++;
		if (i==8) return 0; else return 1;
	}
	else return 0;
}

int wouldflip (int move, int player, int * board, int dir) {
	int c;
	c = move + dir;
	if (board[c] == opponent(player))
		return findbracketingpiece(c+dir, player, board, dir);
	else return 0;
}

int validp (int move) {
	if ((move >= 11) && (move <= 88) && (move%10 >= 1) && (move%10 <= 8))
		return 1;
	else return 0;
}

int findbracketingpiece(int square, int player, int * board, int dir) {
	while (board[square] == opponent(player)) square = square + dir;
	if (board[square] == player) return square;
	else return 0;
}

void initialise(){
	int i;
	running = 1;
	board = (int *)malloc(BOARDSIZE * sizeof(int));
	for (i = 0; i<=9; i++) board[i]=OUTER;
	for (i = 10; i<=89; i++) {
		if (i%10 >= 1 && i%10 <= 8) board[i]=EMPTY; else board[i]=OUTER;
	}
	for (i = 90; i<=99; i++) board[i]=OUTER;
	board[44]=WHITE; board[45]=BLACK; board[54]=BLACK; board[55]=WHITE;
}

void printboard(){
	int row, col;
	printf("    1 2 3 4 5 6 7 8 [%c=%d %c=%d]\n",
			nameof(BLACK), count(BLACK, board), nameof(WHITE), count(WHITE, board));
	for (row=1; row<=8; row++) {
		printf("%d  ", 10*row);
		for (col=1; col<=8; col++)
			printf("%c ", nameof(board[col + (10 * row)]));
		printf("\n");
	}
}

char nameof (int piece) {
	static char piecenames[5] = ".wb?";
	return(piecenames[piece]);
}

int count (int player, int * board) {
	int i, cnt;
	cnt=0;
	for (i=1; i<=88; i++)
		if (board[i] == player) cnt++;
	return cnt;
}



int minmax (int player, int * board, int ply, int (* evalfn) (int, int *)) {
	int i, max, ntm, newscore, bestmove, * moves, * newboard;
	moves = legalmoves(player, board); 
	max = LOSS - 1;  
	for (i=1; i <= moves[0]; i++) {
		newboard = copyboard(board); BOARDS = BOARDS + 1;
		makemove(moves[i], player, newboard);
		ntm = nexttoplay(newboard, player, 0);
		if (ntm == 0) {  
			newscore = diffeval(player, newboard);
			if (newscore > 0) newscore = WIN; 
			if (newscore < 0) newscore = LOSS; 
		}
		if (ntm == player){
			newscore = maxchoice(player, newboard, ply-1, evalfn);
			printf("ntm == player %d \n",newscore);}
		if (ntm == opponent(player)){
			newscore = minchoice(player, newboard, ply-1, evalfn);
			printf("%d --> %d \n",moves[i],newscore);
		}
		if (newscore > max) {
			max = newscore;
			bestmove = moves[i]; 
		}
		free(newboard);
	}
	free(moves);
	return(bestmove);
}

int * copyboard (int * board) {
	int i, * newboard;
	newboard = (int *)malloc(BOARDSIZE * sizeof(int));
	for (i=0; i<BOARDSIZE; i++) newboard[i] = board[i];
	return newboard;
}

int maxchoice (int player, int * board, int ply,
		int (* evalfn) (int, int *)) {
	int i, max, ntm, newscore, * moves, * newboard;
	int minchoice (int, int *, int, int (*) (int, int *));
	if (ply == 0) return((* evalfn) (player, board));
	moves = legalmoves(player, board);
	max = LOSS - 1;
	for (i=1; i <= moves[0]; i++) {
		newboard = copyboard(board); BOARDS = BOARDS + 1;
		makemove(moves[i], player, newboard);
		ntm = nexttoplay(newboard, player, 0);
		if (ntm == 0) {
			newscore = diffeval(player, newboard);
			if (newscore > 0) newscore = WIN;
			if (newscore < 0) newscore = LOSS;
		}
		if (ntm == player)
			newscore = maxchoice(player, newboard, ply-1, evalfn);
		if (ntm == opponent(player))
			newscore = minchoice(player, newboard, ply-1, evalfn);
		if (newscore > max) max = newscore;
		free(newboard);
	}
	free(moves);
	return(max);
}

int minchoice (int player, int * board, int ply,
		int (* evalfn) (int, int *)) {
	int i, min, ntm, newscore, * moves, * newboard;
	if (ply == 0) return((* evalfn) (player, board));
	moves = legalmoves(opponent(player), board);
	min = WIN+1;
	for (i=1; i <= moves[0]; i++) {
		newboard = copyboard(board); BOARDS = BOARDS + 1;
		makemove(moves[i], opponent(player), newboard);
		ntm = nexttoplay(newboard, opponent(player), 0);
		if (ntm == 0) {
			newscore = diffeval(player, newboard);
			if (newscore > 0) newscore = WIN;
			if (newscore < 0) newscore = LOSS;
		}
		if (ntm == player)
			newscore = maxchoice(player, newboard, ply-1, evalfn);
		if (ntm == opponent(player))
			newscore = minchoice(player, newboard, ply-1, evalfn);
		if (newscore < min) min = newscore;
		free(newboard);
	}
	free(moves);
	return(min);
}

void game_over(){
	MPI_Finalize();
}
