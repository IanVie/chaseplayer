Project Description

In this project  implemented an Othello player that uses a distributed (using MPI) tree search -using a minimax to evaluate the available moves from it's current position before making a move - automated thinking .
Othello is a strategy board game for two players. It is played on an nxn board.There are n^2 identical discs, which are typically white on one side and black on the other side. Each player is assigned a colour white/black and always places their disks with their assigned colour facing up.
After placing a disk on the board, all the disks with the opponent's colour facing up, that are in a straight line between the disk just placed and another disk with the current player's colour facing up, are turned over.
When play ends, the winner is the player that has the most disks with it's colour facing up.


